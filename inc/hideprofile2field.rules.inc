{ "rules_login_redirect" : {
    "LABEL" : "login redirect",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "REQUIRES" : [ "rules" ],
    "ON" : { "user_login" : [] },
    "DO" : [ { "redirect" : { "url" : "bio\/[account:uid]" } } ]
  }
}

