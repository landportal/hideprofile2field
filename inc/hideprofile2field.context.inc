$context = new stdClass();
$context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
$context->api_version = 3;
$context->name = 'profile';
$context->description = '';
$context->tag = 'profile';
$context->conditions = array(
  'path' => array(
    'values' => array(
      'bio/*' => 'bio/*',
    ),
  ),
);
$context->reactions = array(
  'block' => array(
    'blocks' => array(
      'hideprofile2field-hideprofile2field_access' => array(
        'module' => 'hideprofile2field',
        'delta' => 'hideprofile2field_access',
        'region' => 'super_content',
        'weight' => '-10',
      ),
      'views-biography_content-related' => array(
        'module' => 'views',
        'delta' => 'biography_content-related',
        'region' => 'sub_content',
        'weight' => '-10',
      ),
      'hideprofile2field-hideprofile2field_started' => array(
        'module' => 'hideprofile2field',
        'delta' => 'hideprofile2field_started',
        'region' => 'sidebar_first',
        'weight' => '-10',
      ),
      'hideprofile2field-hideprofile2field_mail_subs' => array(
        'module' => 'hideprofile2field',
        'delta' => 'hideprofile2field_mail_subs',
        'region' => 'sidebar_second',
        'weight' => '-10',
      ),
      'hideprofile2field-hideprofile2field_group' => array(
        'module' => 'hideprofile2field',
        'delta' => 'hideprofile2field_group',
        'region' => 'sidebar_second',
        'weight' => '-9',
      ),
    ),
  ),
  'template_suggestions' => 'page__bio',
  'theme_html' => array(
    'class' => 'page-bio',
  ),
);
$context->condition_mode = 0;

// Translatables
// Included for use with string extractors like potx.
t('profile');


