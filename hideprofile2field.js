(function($) {

Drupal.behaviors.hideprofile2field = {
  attach: function (context, settings) {
    $('.unpublish', context).click(
      function(event){
        var uid = $(this).attr('data-uid');
        $.post(window.location.origin + Drupal.settings.basePath + "hide/unpublish/" + uid);
        //window.location.reload(true);
        event.preventDefault();
        // to reload the page ONCE all the ajax request has been performed
        $(document).bind("ajaxSend", function () {
                    //console.log("waiting for all requests to complete...");
                    // ajaxStop (Global Event)
                    // This global event is triggered if there are no more Ajax requests being processed.
        }).bind("ajaxStop", function () {
                    // maybe reload here?
                    location.reload();
        });
      }
    );
  }
};

})(jQuery);
