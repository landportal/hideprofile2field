# Hiddenprofile2field Drupal module for LP

* Add the file `profile2.tpl.php` to the folder `sites/all/modules/profile2/`

* Add the file `page--bio.tpl.php` to the folder `sites/landportal.org/themes/lpbs/templates/system`

* Import the content of the file `hideprofile2field.context.inc` into the Context UI

* Import the content of the file `hideprofile2field.rules.inc ` into the Rules UI

### Notes

The file hideprofile2field.css is loaded from file `hideprofile2field.module` and not from  `hideprofile2field.info` to be added just when the form is displayed


